import edu.princeton.cs.algs4.StdIn;
import java.io.File;
import java.io.IOException;
import java.util.Scanner;
import java.util.Arrays;

public class Permutation {


    public static void main(String[] args){
        
        int k = Integer.parseInt(args[0]);
//        File file = new File(args[1]);
//        input = new Scanner(file);

        RandomizedQueue<String> rq = new RandomizedQueue<String>();
        while(!StdIn.isEmpty()){
            rq.enqueue(StdIn.readString());
        }
        int i = 0;
        while(!rq.isEmpty() && i < k){
            System.out.println(rq.dequeue());
            i++;
        }
    }
}