import java.util.Iterator;
import edu.princeton.cs.algs4.StdRandom;
import java.util.NoSuchElementException;

public class RandomizedQueue<Item> implements Iterable<Item> {
    private Item[] a;
    private int n;

    // construct an empty randomized queue
    public RandomizedQueue(){
        a = (Item[]) new Object[2];
        n = 0;
    }

    // is the randomized queue empty?
    public boolean isEmpty(){
        return n == 0;
    }

    // return the number of items on the randomized queue
    public int size(){
        return n;
    }

    // add the item
    public void enqueue(Item item){
        if (item == null) throw new IllegalArgumentException("Attempting to enqueue a null item");
        if (n == a.length) resize(2*a.length);
        a[n++] = item;
    }

    // remove and return a random item
    public Item dequeue(){
        if (isEmpty()) throw new NoSuchElementException("Stack Underflow");
        int index = StdRandom.uniform(0, n);
        Item returned = a[index];
        if(index == n - 1){
            a[index] = null;
        } else {
            a[index] = a[n- 1];
            a[n- 1] = null;
        }
        n--;
        if (n > 0 && n == a.length/4) resize(a.length/2);
        return returned;
    }

    private void resize(int newLength){
        Item[] resizedQueue = (Item[]) new Object[newLength];
        for(int i = 0; i < n; i++){
            resizedQueue[i] = a[i];
        }
        a = resizedQueue;
    }

    // return a random item (but do not remove it)
    public Item sample(){
        if (isEmpty()) throw new NoSuchElementException("Stack Underflow");
        int index = StdRandom.uniform(0, a.length);
        return a[index];
    }

//    OperationCountLimitExceededException
//    Number of calls to next() and hasNext() exceeds limit: 100000000

    private class RandomizedQueueIterator implements Iterator<Item>{
        private int i;

        public RandomizedQueueIterator(){
            i = 0;
        }

        public boolean hasNext(){
            return i < n;
        }

        public void remove() { throw new UnsupportedOperationException(); }

        public Item next(){
            if (!hasNext()) throw new NoSuchElementException("Stack Overflow");
            return dequeue();
        }

    }

    // return an independent iterator over items in random order
    public Iterator<Item> iterator(){ return new RandomizedQueueIterator(); }

    // unit testing (optional)
    public static void main(String[] args){
        RandomizedQueue<Integer> rq = new RandomizedQueue<Integer>();
        rq.isEmpty();
        rq.enqueue(23);
        rq.enqueue(84);
        int size = rq.size();
        System.out.println("the size of the rq is "+size);
        rq.dequeue();
        rq.isEmpty();
        size = rq.size();
        System.out.println("the size of the rq is "+size);
        rq.enqueue(32);
        rq.enqueue(48);
        int sample = rq.sample();
        System.out.println("a sample item of the rq is "+sample);
        Iterator<Integer> iterator = rq.iterator();
        System.out.println("item next kwa iterator ni "+iterator.next());
    }
}