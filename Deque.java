import java.util.Iterator;
import java.lang.IllegalArgumentException;
import java.util.NoSuchElementException;

import edu.princeton.cs.algs4.StdOut;
public class Deque<Item> implements Iterable<Item>{
    private int n;
    private Node first, last, fresh, old;

    private class Node {
        private Item item;
        private Node next;
        private Node previous;
    }

    // construct an empty deque
    public Deque(){
        first = new Node();
        last = new Node();
        first.item = null;
        first.previous = null;
        first.next = last;
        last.item = null;
        last.previous = first;
        last.next = null;
        n = 0;
        assert check();
    }

    private boolean check(){
        if (n < 0) return false;
        if (n == 0){
            if (first.next != last || last.previous != first) return false;
            if (first.previous != null || last.next != null) return false;
        }
        else if(n == 1){
            if (first.next != last.previous) return false;
            if (first.next.item == null || first.next.next != last || first.next.previous != first) return false;
        }
        else {
            if (first.next.item == null || last.previous.item == null) return false;
            if (first.previous != null || last.next != null) return false;
        }
        int numberOfNodes = 0;
        for (Node x = first; x.item != null && numberOfNodes <= n; x = x.next){
            numberOfNodes++;
        }
        if(numberOfNodes != n) return false;

        return true;
    }

    // is the deque empty?
    public boolean isEmpty(){
        return first.next == last && last.previous == first;
    }

    // return the number of items on the deque
    public int size(){
        return n;
    }

    // add the item to the front
    public void addFirst(Item item){
        if(item == null) throw new java.lang.IllegalArgumentException();
        if (n == 0){
            fresh = new Node();
            fresh.item = item;
            fresh.next = last;
            fresh.previous = first;
            first.next = fresh;
            last.previous = fresh;
            n = 1;
            assert check();
            return;
        }
        old = first.next;
        fresh = new Node();
        fresh.item = item;
        fresh.next = old;
        fresh.previous = first;
        old.previous = fresh;
        first.next = fresh;
        n = n + 1;
        assert check();
    }

    // add the item to the end
    public void addLast(Item item){
        if(item == null) throw new java.lang.IllegalArgumentException();
        if (n == 0){
            fresh = new Node();
            fresh.item = item;
            fresh.previous = first;
            fresh.next = last;
            last.previous = fresh;
            first.next = fresh;
            n = 1;
            assert check();
            return;
        }
        old = last.previous;
        fresh = new Node();
        fresh.item = item;
        fresh.next = last;
        fresh.previous = old;
        old.next = fresh;
        last.previous = fresh;
        n = n + 1;
        assert check();
    }

    // remove and return the item from the front
    public Item removeFirst(){
        if (isEmpty()) throw new java.util.NoSuchElementException();
        if (n == 1){
            old = first.next;
            first.next = last;
            last.previous = first;
            n = 0;
            assert check();
            Item returned = old.item;
            old = null;
            return returned;
        }
        old = first.next;
        fresh = old.next;
        first.next = fresh;
        fresh.previous = first;
        n = n - 1;
        assert check();
        Item returned = old.item;
        old = null;
        return returned;
    }

    // remove and return the item from the end
    public Item removeLast(){
//        StdOut.print("before no such exception in "+typeHeld+" ");
        if (isEmpty()) throw new java.util.NoSuchElementException();
        if (n == 1){
            old = last.previous;
            first.next = last;
            last.previous = first;
            n = 0;
            assert check();
            Item returned = old.item;
            old = null;
            return returned;
        }
        old = last.previous;
        fresh = old.previous;
        fresh.next = last;
        last.previous = fresh;
        n = n - 1;
        assert check();
        Item returned = old.item;
        old = null;
        return returned;
    }

    private class DequeIterator implements Iterator<Item>{
        private Node current = first;
        public boolean hasNext(){ return current.next.item != null; }
        public void remove() { throw new java.lang.UnsupportedOperationException(); }
        public Item next(){
            if (current.next.item == null) throw new java.util.NoSuchElementException();
            Item item = current.next.item;
            current = current.next;
            return item;
        }
    }

    // return an iterator over items in order from front to end
    public Iterator<Item> iterator() { return new DequeIterator(); }

    // unit testing (optional)
    public static void main(String[] args) {
        Deque<Integer> rq = new Deque<Integer>();
        rq.isEmpty();
        rq.addFirst(23);
        rq.addLast(84);
        int size = rq.size();
        System.out.println("the size of the deque is "+size);
        rq.removeFirst();
        rq.isEmpty();
        size = rq.size();
        System.out.println("the size of the deque is "+size);
        rq.addLast(32);
        int random = rq.removeLast();
        int sample = rq.size();
        System.out.println("the size of the deque is "+sample);
        Iterator<Integer> iterator = rq.iterator();
        System.out.println("item next kwa iterator ni "+iterator.next());
    }
}